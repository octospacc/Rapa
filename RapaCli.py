#!/usr/bin/env python3

from peewee import *
import markovify
import logging
from sys import argv
from time import sleep
from typing import List
import random
from random import choice
from RapaModules.Config import *
from RapaModules.Database import *
from RapaModules.Locale import *

# Create database file and table if they don't exist
DB.create_tables([SimpleUserMdl, SimpleUserMsgMdl, SimpleGenMsgMdl], safe=True)

def Main():
	Name = None
	Lang = None
	Read = None
	Trigger = None
	TriggerCount = None

	global Locale
	Locale = Locale

	if len(argv) > 1:
		for i in argv:
			i = i.split(':')
			if i[0] == '/Name':
				Name = i[1]
			elif i[0] == '/Language':
				Lang = i[1]
			elif i[0] == '/Read':
				Read = i[1]
			elif i[0] == '/Trigger':
				Trigger = i[1] if len(i) == 2 else True
			elif i[0] == '/TriggerCount':
				TriggerCount = i[1]

	if not Name:
		PrintServMsg(choice(Locale['Generic']['Ask']['Name']))
		Name = str(input(' > '))

	if Name == '' or Name == 'User':
		Name = 'User'
		PrintServMsg('Starting as anonymous user')

	try:
		SimpleUserMdl.get(SimpleUserMdl.Name==Name)
	except Exception:
		SimpleUserMdl.create(Name=Name, Lang=None)

	User = SimpleUserMdl.get(SimpleUserMdl.Name==Name)

	if not Lang and Name != 'User':
		Lang = User.Lang
	if not Lang:
		PrintServMsg(choice(Locale['Generic']['Ask']['Language']))
		Lang = str(input(' > '))
		if Name != 'User':
			SimpleUserMdl.update(Lang=Lang).where(SimpleUserMdl.Name==Name).execute()

	Locale = LoadLocale(Lang)
	if not Locale:
		Locale = LoadLocale('En')
	PrintServMsg(choice(Locale['Generic']['Current']['Language']))

	if Read:
		ReadText(Read, User)
	if Trigger:
		Generate(User, Trigger if type(Trigger)==str else None)

	if not Trigger and not Read:
		while True:
			try:
				HandleInput(User)
			except KeyboardInterrupt:
				Quit()

def PrintServMsg(Text, StartNL=False):
	Lines = Text.splitlines()
	if StartNL:
		print('')
	for l in Lines:
		print('[ {Text} ]'.format(Text=l))

def HandleInput(User:SimpleUserMdl):
	Input = str(input(' > '))
	FirstArg = Input.split(' ')[0].lower()
	CommandHandlers = ReadConf(Conf, 'General', 'Command Handlers')
	if not CommandHandlers:
		CommandHandlers = '/'
	if FirstArg[0] in CommandHandlers:
		if FirstArg[1:] == 'trigger':
			Generate(User, Input[9:])
		elif FirstArg[1:] == 'stats':
			PrintStats(User)
	else:
		ReadText(Input, User)
		Generate(User)

def ReadText(Text, User:SimpleUserMdl):
	if Text.count(' ') > 0:
		with DB.atomic():
			SimpleUserMsgMdl.insert(User=User, Message=Text).execute()

# Generate a response
def GenerateMarkov(Messages:List[str]) -> str:
	if(not Messages):
		return None
	# More messages from db means more complex responses
	if(len(Messages) < 100):
		StateSize = 1
	elif(500 > len(Messages) >= 100):
		StateSize = 2
	else:
		StateSize = 3
	TextModel = markovify.NewlineText(Messages, state_size=StateSize)
	return TextModel.make_short_sentence(1024)

def FetchMsg(User:SimpleUserMdl, Trigger=None):
	if Trigger:
		Messages = SimpleUserMsgMdl.select().where(SimpleUserMsgMdl.User==User & SimpleUserMsgMdl.Message.contains(Trigger))
	else:
		Messages = SimpleUserMsgMdl.select().where(SimpleUserMsgMdl.User==User)
	return [m.Message for m in Messages]

def CheckDuplicated(Message:str, User:SimpleUserMdl):
	Response, Created = SimpleGenMsgMdl.get_or_create(User=User, Message=Message)
	return not Created

def Generate(User:SimpleUserMdl, Trigger=None):
	Message = GenerateMarkov(FetchMsg(User, Trigger))
	if (Message and not CheckDuplicated(Message, User)):
		print(Message)

def PrintStats(User: SimpleUserMdl):
	PrintServMsg(
		"{User}: {UserMsg}, {UserGenMsg}\n{All}\n{Global}".format(
			User=choice(Locale['Generic']['Stats']['User']), UserMsg=User.Messages.count(), UserGenMsg=User.GenMessages.count(),
			All=choice(Locale['Generic']['Stats']['All']),
			Global=choice(Locale['Generic']['Stats']['Global'])))

def Quit():
	PrintServMsg(choice(Locale['Generic']['Ask']['Exit']), StartNL=True)
	try:
		Input = str(input(' > '))
		if Input.lower() == 'y' or Input.lower() == 'ye' or Input.lower() == 'yes':
			exit()
	except KeyboardInterrupt:
		exit()

if __name__ == '__main__':
	Conf = LoadConf('Config.ini')

	Lang = ReadConf(Conf, 'Simple', 'Language')
	if not Lang: Lang = 'En'

	Locale = LoadLocale(Lang)
	if not Locale:
		Locale = LoadLocale('En')
	if not Locale:
		PrintServMsg("Can't load any locale. The program won't start.")
		exit(1)

	Main()
