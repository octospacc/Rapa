/* This script converts the original Peka TriggerResponseBot DB
   to a more flexible and non-platform-centric format */

ALTER TABLE 'generatedmessagemodel'
	RENAME TO 'tggenmsgmdl';
ALTER TABLE 'groupsettings'
	RENAME TO 'tggroupsettingsmdl';
ALTER TABLE 'groupsettings_tgusermodel_through'
	RENAME TO 'tggroupsettingsmdl_tgusermdl_through';
ALTER TABLE 'tggroupsettingsmdl_tgusermdl_through'
	RENAME COLUMN 'groupsettings_id' TO 'tggroupsettingsmdl_id';
ALTER TABLE 'tggroupsettingsmdl_tgusermdl_through'
	RENAME COLUMN 'tgusermodel_id' TO 'tgusermdl_id';
ALTER TABLE 'tgusermodel'
	RENAME TO 'tgusermdl';
ALTER TABLE 'usermessagemodel'
	RENAME TO 'tgusermsgmdl';

DROP INDEX generatedmessagemodel_user_id;
DROP INDEX groupsettings_user_id;
DROP INDEX groupsettingstgusermodelthrough_groupsettings_id;
DROP INDEX groupsettingstgusermodelthrough_groupsettings_id_tgusermodel_id;
DROP INDEX groupsettingstgusermodelthrough_tgusermodel_id;
DROP INDEX usermessagemodel_user_id;
