import configparser
from ast import literal_eval

def LoadConf(File):
	Conf = configparser.ConfigParser()
	Conf.read(File)
	return Conf

def ReadConf(Conf, Section, Option=None):
	if Option == None:
		if Conf.has_section(Section):
			return Conf[Section]
		else:
			return None
	else:
		if Conf.has_option(Section, Option):
			return Conf[Section][Option]
		else:
			return None
