from peewee import *

DB = SqliteDatabase('Rapa.db')

# Base db class, every model inherits the db connector
class BaseMdl(Model):
	class Meta:
		database = DB

# Generic user model
class SimpleUserMdl(BaseMdl):
	Name = CharField()
	Lang = CharField(null=True)

# Storing all generic user messages
class SimpleUserMsgMdl(BaseMdl):
	User = ForeignKeyField(SimpleUserMdl, backref='Messages')
	Message = CharField(max_length=8192)

# Storing already generated generic messages
class SimpleGenMsgMdl(BaseMdl):
	User = ForeignKeyField(SimpleUserMdl, backref='GenMessages')
	Message = CharField(max_length=2048)

# Telegram base user model
class TGUserMdl(BaseMdl):
	# Telegram data
	chat_id = CharField()
	first_name = CharField()
	username = CharField(null=True)
	language_code = CharField(default='en')
	# Markov settings
	# Chances of getting a response automatically. 10/50/90 % /mute for 0
	autoreply_chance = IntegerField(default=0)
	# Fixed chance of replying after 50/200/1000/5000 messages /mute for 0
	autoreply_fixed = IntegerField(default=200)
	# True for random chance | False for fixed
	random_autoreply = BooleanField(default=True)
	markov_algorithm = CharField(default='last_message', choices=['all_messages', 'last_message'])

# Storing all original Telegram messages
class TGUserMsgMdl(BaseMdl):
	user = ForeignKeyField(TGUserMdl, backref='messages')
	message_id = CharField()
	chat_id = CharField()
	message_text = CharField(max_length=4096)

# Storing already generated Telegram messages
class TGGenMsgMdl(BaseMdl):
	user = ForeignKeyField(TGUserMdl, backref='generated_messages')
	chat_id = CharField(default='')
	message_text = CharField(max_length=1024)

# Storing Telegram group admins and settings
class TGGroupSettingsMdl(BaseMdl):
	user = ForeignKeyField(TGUserMdl)
	chat_id = CharField()
	override_settings = BooleanField(default=False)
	admins = ManyToManyField(TGUserMdl, backref='groups')
