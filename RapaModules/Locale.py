import json

def FindLocale(Locale):
	try:
		with open('Locales/FullNames.json', 'r') as File:
			FullNames = json.load(File)
		for i in FullNames:
			if Locale in FullNames[i]:
				return LoadLocale(i)
		return None

	except Exception:
		return None

def LoadLocale(Locale):
	try:
		with open('Locales/{0}.json'.format(Locale), 'r') as File:
			return json.load(File)

	except Exception:
		return FindLocale(Locale)
